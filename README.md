# Ansible Role: Terragrunt

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-terragrunt/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-terragrunt/-/commits/main) 

This role installs [Terragrunt](https://github.com/gruntwork-io/terragrunt/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    terragrunt_version: latest # tag "0.36.0" if you want a specific version
    terragrunt_arch: amd64 # amd64, 386, arm64
    setup_dir: /tmp
    terragrunt_bin_path: /usr/local/bin/terragrunt
    terragrunt_repo_path: https://github.com/gruntwork-io/terragrunt/releases/download

This role can install the latest or a specific version. See [available terragrunt releases](https://github.com/gruntwork-io/terragrunt/releases/) and change this variable accordingly.

Terragrunt supports amd64, 386 arm64 CPU architectures, just change for the main architecture of your CPU.

    terragrunt_arch: amd64 # amd64, 386, arm64

The path of the Terragrunt repository.

    terragrunt_repo_path: https://github.com/gruntwork-io/terragrunt/releases/download

The location where the Terragrunt binary will be installed.

    terragrunt_bin_path: /usr/local/bin/terragrunt

Terragrunt needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Terragrunt. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: terragrunt

## License

MIT / BSD
